<?php

  class Database{

      private static $INSTANCE = null;
      private        $mysqli,
                     $host = 'localhost',
                     $user = 'root',
                     $pass = '',
                     $db   = 'sekolah';



      public function __construct()
      {
        $this->mysqli = new mysqli( $this->host, $this->user, $this->pass, $this->db);

        if( mysqli_connect_error()  )
        {
          echo 'koneksi gagal';
        }
      }


      public static function getInstance(){
        if( !isset(self::$INSTANCE) ){
          self::$INSTANCE = new Database;
        }

        return self::$INSTANCE;
      }

      public function insert($table, $fields = array())
      {

          //ambil kolom
          $column = implode (", ", array_keys($fields));

          //ambil nilai
          $valueArrays = array();
          $i = 0;
          foreach($fields as $key=>$values){
            if( is_int($values) ){

              $valueArrays[$i] = $this->escape($values) ;

            }
            else{

              $valueArrays[$i] = "'" . $this->escape($values) . "'";

            }
            $i++;
          }

          $values = implode (",", $valueArrays);

          $query  = "INSERT INTO $table ($column) VALUES ($values)";

          return $this->run_query($query);

      }


      public function get_info($table, $column, $value){

        if( !is_int($value) ){
          $value = "'".$value."'";
        }


        $query = "SELECT * FROM $table WHERE $column = $value";

        $result = $this->mysqli->query($query);

        while($row = $result->fetch_object()){
          return $row;
        }

      }


      public function run_query($query){

        if($this->mysqli->query($query)){
          return true;
        }
        else{
          return false;
        }

      }

      public function escape($name)
      {
        return $this->mysqli->real_escape_string(htmlentities($name));
      }

  }


  // $mysqli = new mysqli($host, $user, $pass, $db);
  //
  // if($mysqli->connect_errno){
  //   echo 'koneksi gagal'.$mysqli->connect_error;
  // }

  // ==============================> QUERY INSERT & MULTI QUERY <==========================

  // $sql = "INSERT INTO murid (nama, alamat) VALUES ('peppy', 'klatak');";
  // $sql .= "INSERT INTO murid (nama, alamat) VALUES ('loctav', 'oss')";
  // ($mysqli->query($sql))
  //
  // if( ($mysqli->multi_query($sql)) === TRUE ){
  //   echo 'berhasil ';
  // }
  // else{
  //   echo 'gagal'.$mysqli->error;
  // }


  //================================> PREPARE STATEMENT <===========================


  // $query = $mysqli->prepare('INSERT INTO murid (nama, alamat) VALUES (?, ?)');
  // $query->bind_param("ss", $nama, $alamat);
  //
  // $nama = 'adi';
  // $alamat = 'rumah';
  //
  // $query->execute();



  //===============================> SELECT QUERY  <=================================

  // $query  = "SELECT nama , alamat FROM murid ";
  // $result = $mysqli->query($query);
  //
  // if($result->num_rows > 0){
  //   while($data = $result->fetch_object()){
  //     echo $data->nama . ' ' . $data->alamat . ' <br/> ';
  //   }
  // }
  // else{
  //   echo 'data yang anda cari tidak ditemukan';
  // }


  // $mysqli->close(); //untuk menutup koneksi
 ?>
