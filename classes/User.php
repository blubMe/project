<?php


class User{

  private $_db;

  public function __construct()
  {
    $this->_db = Database::getInstance();
  }

  public function register_user($fields = array())
  {
    if( $this->_db->insert('users',$fields) )
    {
      return true;
    }
    else{
      return false;
    }
  }


  public function login_cek($nama){

    $data = $this->_db->get_info('users','username',$nama);
    if(!empty($data)){
      return true;
    }
    else{
      return false;
    }

  }


  public function login_user($nama,$pass)
  {
      $data = $this->_db->get_info('users','username',$nama);

        if(password_verify($pass , $data->password)){
          return true;
        }
        else {
          return false;
        }

  }

}

 ?>
